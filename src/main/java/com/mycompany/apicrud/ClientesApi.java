/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.apicrud;

import com.mycompany.apicrud.dao.ClientesJpaController;
import com.mycompany.apicrud.dao.exceptions.NonexistentEntityException;
import com.mycompany.apicrud.entity.Clientes;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 *
 * @author Andrea
 */

@Path("clientes")
public class ClientesApi {
    
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response listarClientes(){
        
     ClientesJpaController dao = new ClientesJpaController();
     
     List<Clientes> lista = dao.findClientesEntities();
     
     return Response.ok(200).entity(lista).build();
    }
    
    @POST
    @Produces(MediaType.APPLICATION_JSON)
    public Response add(Clientes addcliente){
    
        try {
            ClientesJpaController dao = new ClientesJpaController();
            dao.create(addcliente);
        } catch (Exception ex) {
            Logger.getLogger(ClientesApi.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return Response.ok(200).entity(addcliente).build();
    }
    
    @DELETE
    @Path("/{iddelete}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response delete(@PathParam("iddelete") String iddelete){
        
        try {
            ClientesJpaController dao = new ClientesJpaController();
            dao.destroy(iddelete);
        } catch (NonexistentEntityException ex) {
            Logger.getLogger(ClientesApi.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return Response.ok("Cliente eliminado").build();
    }
    
    @PUT
    @Produces(MediaType.APPLICATION_JSON)
    public Response update(Clientes updatecliente){
      
        try {
            ClientesJpaController dao = new ClientesJpaController();
            dao.edit(updatecliente);
        } catch (Exception ex) {
            Logger.getLogger(ClientesApi.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return Response.ok(200).entity(updatecliente).build();
    }
}
