<%-- 
    Document   : index
    Created on : May 1, 2021, 6:48:10 PM
    Author     : Andrea
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>API CRUD</title>
    </head>
    <body>
        <h1>API CRUD</h1>
        <p>Autor: Andrea Lafertte</p>
        <p>Sección 50</p>
        
        <h1>Usos de esta API y URL</h1>
        <p>URL: https://apicrudciisa.herokuapp.com/</p>
        <p>GET: Recupera o lee los datos. URL GET: https://apicrudciisa.herokuapp.com/api/clientes</p>
        <p>POST: Agrega o crea un cliente. URL POST: https://apicrudciisa.herokuapp.com/api/clientes</p>
        <p>DELETE: Elimina el registro de un cliente. URL DELETE: https://apicrudciisa.herokuapp.com/api/clientes/{iddelete}</p>
        <p>PUT: Edita o actualiza los datos de un cliente. URL PUT: https://apicrudciisa.herokuapp.com/api/clientes</p>

    </body>
</html>
